#!/vendor/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:134217728:661669b4ee5c06cc31af9a5cc47e7f9f88ee8c10; then
  applypatch  \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:100663296:9d5609a725cf51bd110681b8d48c6437aa12ec64 \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:134217728:661669b4ee5c06cc31af9a5cc47e7f9f88ee8c10 && \
      log -t recovery "Installing new oplus recovery image: succeeded" && \
      setprop ro.boot.recovery.updated true || \
      log -t recovery "Installing new oplus recovery image: failed" && \
      setprop ro.boot.recovery.updated false
else
  log -t recovery "Recovery image already installed"
  setprop ro.boot.recovery.updated true
fi
